# haskell-projectroot
Bindings to the projectroot C logic

## Installing
With cabal:
```
$ cabal install projectroot
```

With stack:
```
$ stack install projectroot
```


## Documentation
See the haddock documentation on Hackage.

## License
This code is published under the MIT license.
